/* -*- c++ -*- */

#define NOMA_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "NOMA_swig_doc.i"

%{
#include "NOMA/random_source_c.h"
#include "NOMA/decode_cc.h"
#include "NOMA/BERCounter.h"
#include "NOMA/signalMerger.h"
#include "NOMA/sync_frames_sender.h"
#include "NOMA/sync_frames_receiver.h"
#include "NOMA/resampler_cc.h"
%}

%include "NOMA/random_source_c.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, random_source_c);
%include "NOMA/decode_cc.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, decode_cc);
%include "NOMA/BERCounter.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, BERCounter);

%include "NOMA/signalMerger.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, signalMerger);


%include "NOMA/sync_frames_sender.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, sync_frames_sender);
%include "NOMA/sync_frames_receiver.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, sync_frames_receiver);
%include "NOMA/resampler_cc.h"
GR_SWIG_BLOCK_MAGIC2(NOMA, resampler_cc);
