INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_NOMA NOMA)

FIND_PATH(
    NOMA_INCLUDE_DIRS
    NAMES NOMA/api.h
    HINTS $ENV{NOMA_DIR}/include
        ${PC_NOMA_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    NOMA_LIBRARIES
    NAMES gnuradio-NOMA
    HINTS $ENV{NOMA_DIR}/lib
        ${PC_NOMA_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NOMA DEFAULT_MSG NOMA_LIBRARIES NOMA_INCLUDE_DIRS)
MARK_AS_ADVANCED(NOMA_LIBRARIES NOMA_INCLUDE_DIRS)

