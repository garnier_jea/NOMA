/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_NOMA_BERCOUNTER_H
#define INCLUDED_NOMA_BERCOUNTER_H

#include <NOMA/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace NOMA {

    /*!
     * \brief Compute the Binary Error Rate for N users when provided the expected and decoded results.
     * \ingroup NOMA
     *
     * \details
     * in[0...N-1] reference values\n
     * in[N...2N-1] received values\n
     * out BER\n
     * \n
     * in[0] is compared to in[N], in[1] to in[N+1] and so one.\n
     * 
     */
    class NOMA_API BERCounter : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<BERCounter> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of NOMA::BERCounter.
       *
       * To avoid accidental use of raw pointers, NOMA::BERCounter's
       * constructor is in a private implementation
       * class. NOMA::BERCounter::make is the public interface for
       * creating new instances.
       */
      static sptr make(int nusers, int bufferLength);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_BERCOUNTER_H */

