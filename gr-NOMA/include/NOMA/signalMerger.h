/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_NOMA_SIGNALMERGER_H
#define INCLUDED_NOMA_SIGNALMERGER_H

#include <NOMA/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace NOMA {

    /*!
     * \brief Create a signle symbol from multiple users' symbols using NOMA.
     * \ingroup NOMA
     *
     * \details
     * in[0...N-1] each user's power (in decreasing order of magnitude)\n
     * in[N...2N-1] each user's symbol to transmit\n
     * out the ponderated sum of users' symbols\n
     * \n
     * The output corresponds to in[0] * sqrt(in[N] / 2) + in[1] * sqrt(in[N+1] / 2) + ...\n
     */
    class NOMA_API signalMerger : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<signalMerger> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of NOMA::signalMerger.
       *
       * To avoid accidental use of raw pointers, NOMA::signalMerger's
       * constructor is in a private implementation
       * class. NOMA::signalMerger::make is the public interface for
       * creating new instances.
       */
      static sptr make(int nusers);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_SIGNALMERGER_H */

