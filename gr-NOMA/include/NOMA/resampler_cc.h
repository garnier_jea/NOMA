/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_NOMA_RESAMPLER_CC_H
#define INCLUDED_NOMA_RESAMPLER_CC_H

#include <NOMA/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace NOMA {

    /*!
     * \brief Keeps only 1 symbol out of resamp_ratio, dephased starting at phase_shift * resamp_ratio.
     * \ingroup NOMA
     *
     * \details
     * Allows the user to extract only meaningful values from a given set. The resamp_ratio is typically sps (number of samples per symbol).\n
     * phase_shift helps choosing which value is kept. For example, if resamp_ratio=32 and phase_shift=0.5, then, every 32 values, only the 16th value is kepts (which probably corresponds to the max power).\n
     */
    class NOMA_API resampler_cc : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<resampler_cc> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of NOMA::resampler_cc.
       *
       * To avoid accidental use of raw pointers, NOMA::resampler_cc's
       * constructor is in a private implementation
       * class. NOMA::resampler_cc::make is the public interface for
       * creating new instances.
       */
      static sptr make(int resamp_ratio, float phase_shift);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_RESAMPLER_CC_H */

