/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_NOMA_SYNC_FRAMES_SENDER_H
#define INCLUDED_NOMA_SYNC_FRAMES_SENDER_H

#include <NOMA/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace NOMA {

    /*!
     * \brief Block which initially sends reference symbols and, coupled with a sync_frams_receiver, corrects the phase and amplitude of received symbols.
     * \ingroup NOMA
     *
     * \details
     * This should be at the sending end of a sync_frame_sender.\n
     * This block sends an initial burst of known symbols, later used in sync_frames_receiver to compute the average phase and amplitude difference, and correct it for all the following symbols.\n
     * It sends the initial known symbols (defined in the syncFrames vector of complex values) with a frequency of freq (where freq = sample_rate / sps).\n
     * After sending those symbols, it just acts as a passthrough block. No symbol is dropped by the block (it just prepends some symbols at the start).\n
     * \n
     * WARNING: The values in syncFrames MUST be != 0 (see sync_frames_receiver for details).\n
     */
    class NOMA_API sync_frames_sender : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<sync_frames_sender> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of NOMA::sync_frames_sender.
       *
       * To avoid accidental use of raw pointers, NOMA::sync_frames_sender's
       * constructor is in a private implementation
       * class. NOMA::sync_frames_sender::make is the public interface for
       * creating new instances.
       */
      static sptr make(int sampRate, int freq, std::vector<gr_complex> syncFrames);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_SYNC_FRAMES_SENDER_H */

