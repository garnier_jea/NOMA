/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_NOMA_SYNC_FRAMES_RECEIVER_H
#define INCLUDED_NOMA_SYNC_FRAMES_RECEIVER_H

#include <NOMA/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace NOMA {

    /*!
     * \brief Block which automatically corrects the phase and amplitude of received symbols.
     * \ingroup NOMA
     *
     * \details
     * This should be at the receiving end of a sync_frame_sender.\n
     * This block uses an initial burst of known symbols to compute the average phase and amplitude difference, and corrects it for all the following symbols.\n
     * It expects the initial known symbols (defined in the syncFrames vector of complex values) to be sent with a frequency of freq (where freq = sample_rate / sps).\n
     * The goal of this block is to not drop any symbolNo symbol is dropped by the block (it just prepends some symbols at the start).
     * Increasing procPerSymbol means that, for each known symbol, multiple values will be analyzed to compute the phase and amplitude difference. It is recommended to only increase this if the received symbols stay long enough at the optimal amplitude (i.e. the transition period is small).\n
     * \n
     * WARNING: The values in syncFrames MUST be != 0. Indeed, since this block is meant to be placed after an USRP source, it will initally be given noise by the antenna until the USRP source actually sends symbols. This means we have to find out which symbols are noise. This is done with a simple amplitude threshold on received values.\n
     * TODO: Make the threashold modifiable in the block params. Discard only the first "zeros" (aka noise) received. Ignore zeros in  syncFrames.\n
     */
    class NOMA_API sync_frames_receiver : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<sync_frames_receiver> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of NOMA::sync_frames_receiver.
       *
       * To avoid accidental use of raw pointers, NOMA::sync_frames_receiver's
       * constructor is in a private implementation
       * class. NOMA::sync_frames_receiver::make is the public interface for
       * creating new instances.
       */
      static sptr make(int sampRate, int freq, std::vector<gr_complex> syncFrames, int procPerSymbol);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_SYNC_FRAMES_RECEIVER_H */

