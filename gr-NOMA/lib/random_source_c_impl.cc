/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "random_source_c_impl.h"

#include <complex>
#include <stdlib.h>
#include <time.h>

namespace gr {
  namespace NOMA {

    random_source_c::sptr
    random_source_c::make(int sampRate, int freq, int nusers)
    {
      return gnuradio::get_initial_sptr
        (new random_source_c_impl(sampRate, freq, nusers));
    }

    /*
     * The private constructor
     */
    random_source_c_impl::random_source_c_impl(int sampRate, int freq, int nusers)
      : gr::sync_block("random_source_c",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(nusers, nusers, sizeof(gr_complex))),
      sampRate(sampRate),
      freq(freq),
      nusers(nusers)
    {
      /* initialize random seed: */
      srand(time(NULL));
      this->t = this->sampRate / this->freq;
      this->lastSymbols = std::vector<gr_complex>();
    }

    /*
     * Our virtual destructor.
     */
    random_source_c_impl::~random_source_c_impl()
    {
    }

    int
    random_source_c_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      // For each expected value
      for (int i = 0; i < noutput_items; ++i)
      {
        // Update the "time"
        this->t += 1;

        // Change output symbols if we reached the time to switch
        if (this->t >= this->sampRate / this->freq)
        {
          this->t -= this->sampRate / this->freq;
          lastSymbols = std::vector<gr_complex>();

          // For each user, compute a new symbol
          for (int user = 0; user < nusers; ++user)
          {
            int bit1 = (rand() % 2) * 2 - 1;
            int bit2 = (rand() % 2) * 2 - 1;
            lastSymbols.push_back(gr_complex(bit1, bit2));
          }
        }

        // Add this symbol to the output
        for (int user = 0; user < nusers; ++user)
        {
            gr_complex *out = (gr_complex *) output_items[user];
            out[i] = lastSymbols[user];
        }
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace NOMA */
} /* namespace gr */
