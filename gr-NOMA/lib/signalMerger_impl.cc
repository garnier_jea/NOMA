/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "signalMerger_impl.h"

namespace gr {
  namespace NOMA {

    signalMerger::sptr
    signalMerger::make(int nusers)
    {
      /* Input */
      std::vector<int> inSizes;

      // First we need the value of each power used to send the signal
      for (int i = 0; i < nusers; ++i)
      {
        inSizes.push_back(sizeof(float));
      }

      // Then we need all the signals
      // Note: we could just add one gr_complex, its size would then applied to all
      //       following items as per the doc
      for (int i = 0; i < nusers; ++i)
      {
        inSizes.push_back(sizeof(gr_complex));
      }

      gr::io_signature::sptr in = io_signature::makev(2 * nusers, 2 * nusers, inSizes);
      gr::io_signature::sptr out = io_signature::make(1, 1, sizeof(gr_complex));
      return gnuradio::get_initial_sptr
        (new signalMerger_impl(nusers, in, out));
    }

    /*
     * The private constructor
     */
    signalMerger_impl::signalMerger_impl(int nusers, gr::io_signature::sptr in, gr::io_signature::sptr out)
      : gr::sync_block("signalMerger", in, out),
        nusers(nusers)
    {}

    /*
     * Our virtual destructor.
     */
    signalMerger_impl::~signalMerger_impl()
    {
    }

    int
    signalMerger_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      for (int i = 0; i < noutput_items; ++i)
      {
        // Initialize the complex signal
        gr_complex sig(0, 0);

        // Add the ponderated value of each input
        for (int j = 0; j < nusers; ++j)
        {
          float P = *((float *) input_items[j]); // Power allocated for this signal
          float r = sqrt(P / 2); // Compute the amplitude based on the power
          gr_complex *in = (gr_complex *) input_items[j + nusers];
          sig += gr_complex(r * in[i].real(), r * in[i].imag());
        }

        // Send the signal to the output
        gr_complex *out = (gr_complex *) output_items[0];
        out[i] = sig;
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace NOMA */
} /* namespace gr */

