/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_NOMA_RESAMPLER_CC_IMPL_H
#define INCLUDED_NOMA_RESAMPLER_CC_IMPL_H

#include <NOMA/resampler_cc.h>

namespace gr {
  namespace NOMA {

    class resampler_cc_impl : public resampler_cc
    {
     private:
      int resamp_ratio;
      int symbols_to_skip_left;  // Number of symbols to skip until next output

     public:
      resampler_cc_impl(int resamp_ratio, float phase_shift);
      ~resampler_cc_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_RESAMPLER_CC_IMPL_H */

