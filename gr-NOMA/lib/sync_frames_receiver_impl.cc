/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <cmath>
#include <math.h>
#include <numeric>
#include "sync_frames_receiver_impl.h"

namespace gr {
  namespace NOMA {

    sync_frames_receiver::sptr
    sync_frames_receiver::make(int sampRate, int freq, std::vector<gr_complex> syncFrames, int procPerSymbol)
    {
      return gnuradio::get_initial_sptr
        (new sync_frames_receiver_impl(sampRate, freq, syncFrames, procPerSymbol));
    }

    /*
     * The private constructor
     */
    sync_frames_receiver_impl::sync_frames_receiver_impl(int sampRate, int freq, std::vector<gr_complex> syncFrames, int procPerSymbol)
      : gr::block("sync_frames_receiver",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
        sampRate(sampRate),
        freq(freq),
        syncFrames(syncFrames),
        procPerSymbol(procPerSymbol)
    {
      this->didFinishSync = false;
      this->receivedsymbolsCount = 0;
      this->averagePhaseDifference = 0;
      this->averageModulusRatio = 1;
      this->lastProcIndex = 1; // Index of the last processed value during sync. Start at 1 to ingore extrem values.

      // Make sure we don't try to process more values per symbol than possible
      int repeat = this->sampRate / this->freq;
      this->procPerSymbol = std::min(this->procPerSymbol, repeat - 2);
    }

    /*
     * Our virtual destructor.
     */
    sync_frames_receiver_impl::~sync_frames_receiver_impl()
    {
    }

    void
    sync_frames_receiver_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = noutput_items;
    }

    int
    sync_frames_receiver_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      // Number of outputs that shouldn't be sent
      int skipped = 0;

      // If the sync phase is done, dephase the symbols and send them out
      if (this->didFinishSync)
      {
        for (int i = 0; i < noutput_items; i++)
        {
          // Change phase and modulus of received complex according to averagePhaseDifference and averageModulusRatio
          out[i] = this->correct_symbol(in[i]);
        }
      }
      else
      {
        for (int i=0; i < noutput_items; i++)
        {
          // if we finished the sync phase, correct the input and send it out
          if (this->didFinishSync)
          {
            // Change phase and modulus of received complex according to averagePhaseDifference and averageModulusRatio
            out[i - skipped] = this->correct_symbol(in[i]);
          }
          // If the symbol looks like noise, we don't use it
          else if (sqrt(pow(in[i].real(), 2) + pow(in[i].imag(), 2)) < 0.4)
          {
            // Just consider this to be noise
            skipped += 1;
          }
          else
          {
            skipped += 1;

            // Find out if we should process this value
            // Note: we cut the interval in this->procPerSymbol + 2 values and ignore the first
            //       and last one (they'll most likely be in the slope since the signal isn't perfectly square)
            int repeat = this->sampRate / this->freq; // Number of times a frame is repeated
            int numberToIgnore = (repeat - this->procPerSymbol - 2) / (this->procPerSymbol + 1); // Number of values to ignore between each processed value

            // When changing symbol, reset the last processed index
            bool changedSymbol = this->receivedsymbolsCount % repeat == 0; // Check whether this is the first value of a new symbol
            if (changedSymbol)
            {
              this->lastProcIndex = 1;
            }

            // When we reach a new index to process, do so if it's not the first nor last index
            if (this->lastProcIndex <= this->procPerSymbol
                && this->lastProcIndex * (numberToIgnore + 1) <= this->receivedsymbolsCount % repeat)
            {
              // Note we processed this index
              this->lastProcIndex += 1;

              // Find which frame we expect
              int currentIndex = this->receivedsymbolsCount / repeat;
              gr_complex frame = this->syncFrames[currentIndex];

              /* Phase */
              // Compute phase of each complex and measure the difference between them
              // The average of all differences will then be used as reference
              float phi = atan2(in[i].imag(), in[i].real());
              float expectedPhi = atan2(frame.imag(), frame.real());

              // To compute the difference, get both angles between 0 and 2pi
              float deltaPhi = modulo(expectedPhi, 0, 2 * M_PI) - modulo(phi, -M_PI, M_PI);

              // Then get the difference between 0 and 2pi so the average is correct
              // so that if one is 2pi and the other one 0, the average won't be pi
              deltaPhi = modulo(deltaPhi, -M_PI, M_PI);

              // Save this average
              this->phaseDifferences.push_back(deltaPhi);

              /* Modulus */
              // Compute the modulus
              float modulus = sqrt(pow(in[i].imag(), 2) + pow(in[i].real(), 2));
              float expectedModulus = sqrt(pow(frame.imag(), 2) + pow(frame.real(), 2));
              float ratio = modulus / expectedModulus;
              this->modulusRatios.push_back(ratio);
            }

            // Update the number of symbols received
            this->receivedsymbolsCount += 1;

            // Check if the sync phase is finished
            this->didFinishSync = this->receivedsymbolsCount >= this->syncFrames.size() * repeat;
        
            // If the sync is finished, compute the average
            if (this->didFinishSync)
            {
              this->averagePhaseDifference = this->average(this->phaseDifferences);
              this->averageModulusRatio = this->average(this->modulusRatios);
            }
          }
        }
      }

      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each(noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items - skipped;
    }

    gr_complex sync_frames_receiver_impl::correct_symbol(gr_complex c)
    {
      // Change phase of received complex according to phaseShift
      // (x + jy) * exp(j*Phi) = x*cos(Phi) - y*sin(Phi) + j*(x*sin(Phi) + y*cos(Phi))
      float x = c.real();
      float y = c.imag();
      float real = x * cos(averagePhaseDifference) - y * sin(averagePhaseDifference);
      float imag = x * sin(averagePhaseDifference) + y * cos(averagePhaseDifference);
      return gr_complex(real / averageModulusRatio, imag / averageModulusRatio);
      // return gr_complex(real, imag);
    }

    float sync_frames_receiver_impl::average(std::vector<float> phases)
    {
      // Received all symbols to sync, compute the average phase error
      return std::accumulate(phases.begin(), phases.end(), 0.0) / phases.size();
    }

    float sync_frames_receiver_impl::modulo(float number, float inf, float sup)
    {
      float diff = sup - inf;

      while (number < inf)
      {
        number += diff;
      }

      while (number >= sup)
      {
        number -= diff;
      }

      return number;
    }

  } /* namespace NOMA */
} /* namespace gr */

