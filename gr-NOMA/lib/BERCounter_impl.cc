/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "BERCounter_impl.h"

namespace gr {
  namespace NOMA {

    BERCounter::sptr
    BERCounter::make(int nusers, int bufferLength)
    {
      return gnuradio::get_initial_sptr
        (new BERCounter_impl(nusers, bufferLength));
    }

    /*
     * The private constructor
     */
    BERCounter_impl::BERCounter_impl(int nusers, int bufferLength)
      : gr::sync_block("BERCounter",
              gr::io_signature::make(2 * nusers, 2 * nusers, sizeof(gr_complex)), // nusers first items are compared to nusers next items
              gr::io_signature::make(nusers, nusers, sizeof(float))),
        nusers(nusers),
        nProcessedItems(0),
        bufferLength(bufferLength),
        bufferIndex(0)
    {
      // init error count
      for (int user_idx(0); user_idx < this->nusers; user_idx ++) {
        std::vector<int> errorCountRow(this->bufferLength, 0); // nuser nb of 0's per row
        this->errorCount.push_back(errorCountRow);
      }
    }

    /*
     * Our virtual destructor.
     */
    BERCounter_impl::~BERCounter_impl()
    {
    }

    int
    BERCounter_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      // For each input port
      for (int i = 0; i < nusers; ++i)
      {
        gr_complex *first = (gr_complex *) input_items[i];
        gr_complex *second = (gr_complex *) input_items[nusers + i];
        float *out = (float *) output_items[i];


        // For each input item in the port
        for (int j = 0; j < noutput_items; ++j)
        {
          // Reset errorcount at bufferindex
          this->errorCount[i][this->bufferIndex] = 0;

          // Find the two items to compare and update the error count
          if (first[j].real() != second[j].real())
          {
            errorCount[i][this->bufferIndex] += 1;
          }
          if (first[j].imag() != second[j].imag())
          {
            errorCount[i][this->bufferIndex] += 1;
          }
          
          // Compute error rate
          int count = 0;
          for (int buffer_idx = 0; buffer_idx < this->bufferLength; buffer_idx ++) {
            count += this->errorCount[i][buffer_idx];
          }
          float errorRate = (float) count / (float) (2 * this->bufferLength);

          // Send the BER to the output
          out[j] = errorRate;
        }
      }

      // Update the buffer index
      this->bufferIndex = (this->bufferIndex + 1) % this->bufferLength;

      // Update the number of processed items (2 per complex number)
      nProcessedItems += 2 * noutput_items;

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace NOMA */
} /* namespace gr */

