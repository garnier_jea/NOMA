/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/sync_block.h>
#include <pmt/pmt.h>
#include "decode_cc_impl.h"

namespace gr {
  namespace NOMA {

    decode_cc::sptr
    decode_cc::make(int nusers)
    {
      /* Input */
      std::vector<int> inSizes;

      // First we need the value of each power used to send the signal
      for (int i = 0; i < nusers; ++i)
      {
        inSizes.push_back(sizeof(float));
      }

      // Then we need the signal itself
      inSizes.push_back(sizeof(gr_complex));

      /* Output */
      std::vector<int> outSizes;

      // There are as many outputs as there are users
      // Note: we could just add one gr_complex, its size would then applied to all
      //       following items as per the doc
      for (int i = 0; i < nusers; ++i)
      {
        outSizes.push_back(sizeof(gr_complex));
      }

      gr::io_signature::sptr in = io_signature::makev(nusers + 1, nusers + 1, inSizes);
      // gr::io_signature::make(nusers, nusers, outSizes)
      gr::io_signature::sptr out = io_signature::makev(nusers, nusers, outSizes);

      return gnuradio::get_initial_sptr
        (new decode_cc_impl(nusers, in, out));
    }

    /*
     * The private constructor
     */
    decode_cc_impl::decode_cc_impl(int nusers, gr::io_signature::sptr in, gr::io_signature::sptr out):
      sync_block("decode_cc", in, out),
      nusers(nusers)
    {}

    /*
     * Our virtual destructor.
     */
    decode_cc_impl::~decode_cc_impl()
    {}

    int
    decode_cc_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      // Get the input signal
      const gr_complex *in = (const gr_complex *) input_items[nusers];

      // For each input item in the ports
      for (int i = 0; i < noutput_items; ++i)
      {
        // Get the matching complex signal
        gr_complex sig = in[i];

        // Decode the signal for each user
        for (int j = 0; j < nusers; ++j)
        {
          gr_complex *out = (gr_complex *) output_items[j];
          out[i] = decode(sig);

          // Remove the decoded value from the signal
          float P = *((float *) input_items[j]); // Power allocated for this signal
          float r = sqrt(P / 2); // Compute the amplitude based on the power
          sig -= gr_complex(r * out[i].real(), r * out[i].imag());
        }
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    gr_complex decode_cc_impl::decode(gr_complex in)
    {
      // Simple Gray decoder
      if (in.real() > 0 && in.imag() > 0)
      {
        return gr_complex(1, 1);
      }
      else if (in.real() < 0 && in.imag() > 0)
      {
        return gr_complex(-1, 1);
      }
      else if (in.real() > 0 && in.imag() < 0)
      {
        return gr_complex(1, -1);
      }
      else if (in.real() < 0 && in.imag() < 0)
      {
        return gr_complex(-1, -1);
      }
        
        return gr_complex(0, 0);
    }

  } /* namespace NOMA */
} /* namespace gr */

