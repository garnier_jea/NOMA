/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_NOMA_DECODE_CC_IMPL_H
#define INCLUDED_NOMA_DECODE_CC_IMPL_H

#include <NOMA/decode_cc.h>
#include <gnuradio/io_signature.h>

namespace gr {
  namespace NOMA {

    class decode_cc_impl : public decode_cc
    {
     private:
      int nusers;

     public:
      decode_cc_impl(int nusers, gr::io_signature::sptr in, gr::io_signature::sptr out);
      ~decode_cc_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);


      gr_complex decode(gr_complex in);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_DECODE_CC_IMPL_H */

