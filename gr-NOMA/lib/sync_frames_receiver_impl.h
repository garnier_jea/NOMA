/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_NOMA_SYNC_FRAMES_RECEIVER_IMPL_H
#define INCLUDED_NOMA_SYNC_FRAMES_RECEIVER_IMPL_H

#include <NOMA/sync_frames_receiver.h>

namespace gr {
  namespace NOMA {

    class sync_frames_receiver_impl : public sync_frames_receiver
    {
     private:
      int sampRate;
      int freq;
      int procPerSymbol;  // Number of values to be processed per symbol
      int lastProcIndex;
      bool didFinishSync;
      int receivedsymbolsCount; // Counter to know how many symbols where received during sync phase
      float averagePhaseDifference;
      float averageModulusRatio;
      std::vector<float> phaseDifferences; // List of difference between received and expected phase during sync
      std::vector<float> modulusRatios; // List of ratios between received and expected amplitude during sync
      std::vector<gr_complex> syncFrames;

     public:
      sync_frames_receiver_impl(int sampRate, int freq, std::vector<gr_complex> syncFrames, int procPerSymbol);
      ~sync_frames_receiver_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

      gr_complex correct_symbol(gr_complex c);
      float average(std::vector<float> phases);
      float modulo(float number, float inf, float sup); // Modulo number until it's in [inf, sup[
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_SYNC_FRAMES_RECEIVER_IMPL_H */

