/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_NOMA_RANDOM_SOURCE_C_IMPL_H
#define INCLUDED_NOMA_RANDOM_SOURCE_C_IMPL_H

#include <NOMA/random_source_c.h>

namespace gr {
  namespace NOMA {

    class random_source_c_impl : public random_source_c
    {
     private:
      int sampRate;
      int freq;
      int nusers;
      int t; // Counter to know when to change symbol according to frequency
      std::vector<gr_complex> lastSymbols;

     public:
      random_source_c_impl(int sampRate, int freq, int nusers);
      ~random_source_c_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace NOMA
} // namespace gr

#endif /* INCLUDED_NOMA_RANDOM_SOURCE_C_IMPL_H */
