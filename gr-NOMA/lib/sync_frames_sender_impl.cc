/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "sync_frames_sender_impl.h"

namespace gr {
  namespace NOMA {

    sync_frames_sender::sptr
    sync_frames_sender::make(int sampRate, int freq, std::vector<gr_complex> syncFrames)
    {
      return gnuradio::get_initial_sptr
        (new sync_frames_sender_impl(sampRate, freq, syncFrames));
    }

    /*
     * The private constructor
     */
    sync_frames_sender_impl::sync_frames_sender_impl(int sampRate, int freq, std::vector<gr_complex> syncFrames)
      : gr::block("sync_frames_sender",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
        sampRate(sampRate),
        freq(freq),
        syncFrames(syncFrames)
    {
      this->didFinishSync = false;
      this->t = 0;
      this->syncFrameIndex = 0;
    }

    /*
     * Our virtual destructor.
     */
    sync_frames_sender_impl::~sync_frames_sender_impl()
    {
    }

    void
    sync_frames_sender_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      if (this->didFinishSync)
      {
        // Sync phase is done, this acts as a simple passthrough
        ninput_items_required[0] = noutput_items;
      }
      else
      {
        // Currently in sync phase, only need items if it will finish "soon"
        int itemsLeft = (this->syncFrames.size() - this->syncFrameIndex) - this->t * this->freq / this->sampRate;
        if (itemsLeft > noutput_items)
        {
          ninput_items_required[0] = 0;
        }
        else
        {
          ninput_items_required[0] = noutput_items - itemsLeft;
        }
      }
      ninput_items_required[0] = noutput_items;
    }

    int
    sync_frames_sender_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      // Count the number of items consumed
      // (None are consumed during the sync phase)
      int consumed = 0;

      // For each expected value
      for (int i = 0; i < noutput_items; ++i)
      {
        if (this->didFinishSync)
        {
          // Just send the input to the output
          out[i] = in[consumed];
          consumed += 1;
        }
        else
        {
          // Send the sync frame
          out[i] = this->syncFrames[this->syncFrameIndex];

          // Update the "time"
          this->t += 1;

          // Change output symbols if we reached the time to switch
          if (this->t >= this->sampRate / this->freq)
          {
            // Decrement the time to send this symbol the appropriate number of times
            this->t -= this->sampRate / this->freq;

            // Update the sync index and check if the sync phase is finished
            this->syncFrameIndex += 1;
            this->didFinishSync = this->syncFrameIndex >= this->syncFrames.size();
          }
        }
      }

      // Tell runtime system how many input items we consumed on each input stream.
      consume_each(consumed);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace NOMA */
} /* namespace gr */

