/* -*- c++ -*- */
/* 
 * Copyright 2019 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "resampler_cc_impl.h"

namespace gr {
  namespace NOMA {

    resampler_cc::sptr
    resampler_cc::make(int resamp_ratio, float phase_shift)
    {
      return gnuradio::get_initial_sptr
        (new resampler_cc_impl(resamp_ratio, phase_shift));
    }

    /*
     * The private constructor
     */
    resampler_cc_impl::resampler_cc_impl(int resamp_ratio, float phase_shift)
      : gr::block("resampler_cc",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
        resamp_ratio(resamp_ratio)
    {
      // The first time arround, because of phase shift, we should wait a percentage
      // of resamp_ratio before sending a symbol
      // Note: phase_shift can be > 1, which would mean skipping a few symbols the first time
      symbols_to_skip_left = int(resamp_ratio * phase_shift);
    }

    /*
     * Our virtual destructor.
     */
    resampler_cc_impl::~resampler_cc_impl()
    {
    }

    void
    resampler_cc_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      // We need symbols_to_skip_left to generate the next symbol
      // And then we need (noutput_items - 1) * resamp_ratio to generate the remaining symbols
      ninput_items_required[0] = symbols_to_skip_left + (noutput_items - 1) * resamp_ratio;
    }

    int
    resampler_cc_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];
      int nitems = ninput_items[0];

      int read_index = symbols_to_skip_left;  // The index of the next item to used
      int write_index = 0;  // How many items we already generated

      while (read_index < nitems) {
        out[write_index] = in[read_index];  // Write this item to the output
        write_index += 1;
        read_index += resamp_ratio;  // We need to skip resamp_ratio until the next item
      }

      // Remember how many items we need to skip next time
      symbols_to_skip_left = read_index - nitems;

      // We consumed as many items as read_index
      consume_each(nitems);

      // Tell runtime system how many output items we produced
      return write_index;
    }

  } /* namespace NOMA */
} /* namespace gr */
