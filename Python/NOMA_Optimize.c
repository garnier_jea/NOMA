#define PY_SSIZE_T_CLEAN
#include <Python.h>


double sum(PyObject *P) {
    /* 
     * Returns the sum of all elements in P
     */

    int len = PyList_Size(P);
    double total = 0;

    for (int i = 0; i < len; i++) {
        total += PyFloat_AsDouble(PyList_GetItem(P, i));
    }

    return total;
}

int should_continue(PyObject *P, int i, double Pmax, double step) {
    /* 
     * Returns True if another value of power for i should be tested
     */

    int len = PyList_Size(P);
    double last = PyFloat_AsDouble(PyList_GetItem(P, len - 1));
    double total = sum(P);

    // Make sure we don't use more than the max allocated power
    if (total + last + step > Pmax) {
        return 0;
    }

    // Make sure the power will remain smaller than the one of the previous user
    double Pi = PyFloat_AsDouble(PyList_GetItem(P, i));
    if (i == len - 1) {
        // This is the first-but-one user (the last user's power is not in P since their power can be deduced)
        // Just don't take-up more than half of the total power
        return Pi < (Pmax / 2 - step);
    } else {
        // Make sure that, after adding step, the power is still smaller than the one of the previous user
        double PiplusOne = PyFloat_AsDouble(PyList_GetItem(P, i + 1));
        return Pi < PiplusOne - step;
    }
}

void rec(PyObject *P, PyObject *func, int i, double Pmax, double step, double *BERmin, PyObject **Pmin) {
    /* 
     * Recursive method to compute all the BER values
     */

    int len = PyList_Size(P);
    if (i < 0 || i >= len) {
        return;
    }

    PyObject *arglist;
    PyObject *result;

    while (should_continue(P, i, Pmax, step)) {
        // Increment the power
        double Pn = PyFloat_AsDouble(PyList_GetItem(P, i));
        Pn += step;
        PyList_SetItem(P, i, PyFloat_FromDouble(Pn));

        // Temporarly add the last user's power to the list
        PyList_Append(P, PyFloat_FromDouble(Pmax - sum(P)));

        // Ask Python program to compute the new BER
        arglist = Py_BuildValue("(O)", P);
        result = PyObject_CallObject(func, arglist);
        double newBER = PyFloat_AsDouble(result);

        if (*BERmin < 0 || newBER < *BERmin) {
            *BERmin = newBER;
            *Pmin = PyList_GetSlice(P, 0, len + 1);
        }

        // Remove the last user's power from the list
        // Note: this doesn't reduce the list's size, but it doesn't really matter in our case
        Py_SIZE(P) -= 1;

        // Recursively call ourselves with a copy of P
        rec(PyList_GetSlice(P, 0, len), func, i - 1, Pmax, step, BERmin, Pmin);
    }
}

static PyObject *optimize(PyObject *self, PyObject *args) {
    PyObject *func;
    const int N;
    const double Pmax;
    const double step;

    if (!PyArg_ParseTuple(args, "Oidd", &func, &N, &Pmax, &step)) {
        PyErr_SetString(PyExc_TypeError, "Expected 4 parameters: f, N, Pmax and step");
        return NULL;
    }

    if (!PyCallable_Check(func)) {
        PyErr_SetString(PyExc_TypeError, "f must be callable");
        return NULL;
    }

    if (N < 2) {
        PyErr_SetString(PyExc_TypeError, "N must be greater than 1");
        return NULL;
    }

    if (Pmax <= 0) {
        PyErr_SetString(PyExc_TypeError, "Pmax must be greater than 0");
        return NULL;
    }

    if (step <= 0) {
        PyErr_SetString(PyExc_TypeError, "step must be greater than 0");
        return NULL;
    }

    // Add a reference to the passed function
    Py_XINCREF(func);

    // Init the list of powers with zeros
    PyObject *P = PyList_New(N - 1);
    for (int i = 0; i < N - 1; ++i) {
        PyList_SetItem(P, i, PyFloat_FromDouble(0));
    }

    // Init the return values
    double BERmin = -1;
    PyObject *Pmin = PyList_New(N);

    // Recursively find the min
    rec(P, func, N - 2, Pmax, step, &BERmin, &Pmin);

    if (BERmin < 0) {
        PyErr_SetString(PyExc_TypeError, "Failed to optimize f");
        return NULL;
    }

    // Return the best list we found
    return Pmin;
}

static PyMethodDef OptimizeMethods[] = {
    {"optimize",  optimize, METH_VARARGS, "Find the minimum of the given method."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef optimizemodule = {
    PyModuleDef_HEAD_INIT,
    "NOMA_Optimize",
    NULL,
    -1,
    OptimizeMethods
};

PyMODINIT_FUNC PyInit_NOMA_Optimize(void) {
	return PyModule_Create(&optimizemodule);
}
