import math
import matplotlib.pyplot as plt
import numpy as np
import Model_N_receivers as NOMAStat
import Theory_N_receivers as NOMATh


def compLogLog(N=2, n=1, xlog=True, ylog=True, it=10000, npoints=100):
    """
    Graph of the Bit Error Rate depending on the noise value for a given user
    N: number of users
    n: user for which the SNR and BER are computed
    it: number of iterations for the statistical approach
    """
    Pmax = 10000
    g = 1

    # List of each user's power
    # Each user takes up 85% of the remaining power (up to the last-but-one user)
    P = []
    for i in range(N - 1):
        P = [0.85 * (Pmax - sum(P))] + P

    # The last uses takes up the whole remaining power
    P = [Pmax - sum(P)] + P

    # The power of the user for which the graph is displayed
    Pn = P[n - 1]

    # Find, depending on the value of sigma for the noise, the BER for the nth-user
    Errors = []
    Probas = []
    Sigmas = np.linspace(math.sqrt(Pn / 50), math.sqrt(Pn), npoints)
    for sigma in Sigmas:
        probas = NOMATh.theory(g, sigma, P, N)
        errors = NOMAStat.stats(g, sigma, P, N, it)
        Probas.append(probas[n - 1])
        Errors.append(errors[n - 1])

    # Draw the graph
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    # Set wether the scales should be logarithmic or not
    if xlog:
        ax.set_xscale("log")
        plt.xlabel("SNR user {} (log)".format(n))
    else:
        plt.xlabel("SNR user {}".format(n))

    if ylog:
        ax.set_yscale("log")
        plt.ylabel("BER user {} (log)".format(n))
    else:
        plt.ylabel("BER user {}".format(n))

    # Convert the sigmas for the noise to an RSB value
    RSB = Pn / np.array(np.square(Sigmas))

    ax.plot(RSB, Probas, label="Theoretical error rate")
    ax.plot(RSB, Errors, label="Monte-Carlo error rate", marker="x", ls="")
    plt.legend()
    plt.show()


def compPowerRepartitions(N=2, sigma=0.25, g=1, Pmax=1, it=2000):
    """
    Plot the total BER for different repartitions of power
    """
    # Convert the passed sigma to a value comparable to the one in GNURadio
    # See https://www.gnuradio.org/doc/doxygen/classgr_1_1analog_1_1noise__source__c.html#a6f77085e298f93b770ab0693342d1dbb
    sigma /= math.sqrt(2)

    # Compute which values of power repartition should be tested
    Powers = np.linspace(Pmax / 2, 0.99 * Pmax, 50)

    # Compute the BER and theoritical error probability for each possibility
    Errors = []
    Probas = []
    for P2 in Powers:
        P = [Pmax - P2, P2]
        probas = NOMATh.theory(g, sigma, P, N)
        errors = NOMAStat.stats(g, sigma, P, N, it)
        Probas.append(sum(probas))
        Errors.append(sum(errors))

    # Plot the result
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    plt.xlabel("P2/Pmax")
    plt.ylabel("Sum of BER")

    P = Powers / Pmax

    ax.plot(P, Probas, label="Theoretical error probability")
    ax.plot(P, Errors, label="Statistical error rate", marker="x", ls="")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    compLogLog(N=3, n=1, it=50000, npoints=150)
