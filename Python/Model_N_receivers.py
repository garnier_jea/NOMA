import math
import matplotlib.pyplot as plt
import numpy as np


def sgn(s, i):
    """
    Return 1 if i is in the list s, returns -1 otherwise
    """
    return 2 * (i in s) - 1


def generate(g, sigma, P, N):
    """
    Generate a random signal with noise and attenuation
    """
    # Randomly generate the sign of each symbol
    S = np.random.choice([0, 1], N)

    # Find the indices with a positive sign
    signes = [i + 1 for i in range(len(S)) if S[i] == 1]

    # Knowing the signs, we can compute the values of the amplitude for each user
    r = [math.sqrt(P[i]) * sgn(signes, i + 1) for i in range(len(P))]

    # Sum the signals (to do NOMA)
    x = sum(r)

    # Add the noise and the attenuation
    n = np.random.normal(0, sigma)
    y = x * g + n

    # Returns the signal and what we expect to decode
    return y, r


def decode(y, g, P, N):
    """
    Decode a signal, where y is the sum of the N signals, with a power of P and an attenuation of g
    """
    r_decoded = []

    for i in range(N, 0, -1):
        # Decode the bit at index i
        if y > 0:
            r_decoded.insert(0, g * math.sqrt(P[i - 1]))
        else:
            r_decoded.insert(0, -g * math.sqrt(P[i - 1]))

        # Remove this decoded value from the interpreted value
        y -= r_decoded[0]

    # Return the list of decoded values
    return r_decoded


def stats(g, sigma, P, N, it):
    """
    Use the Monte Carlo method to find an approximate BER
    """
    # The list of results: 1 if we decoded corrected, 0 otherwise
    result = []

    # Do it tests
    for _ in range(it):
        result.append([])

        # Generate the signal and then decode it
        y, r = generate(g, sigma, P, N)
        r_decode = decode(y, g, P, N)

        # Compare what was encoded to what was decoded
        for i in range(N, 0, -1):
            # If both symbols have the same sign, the value was decoded correctly
            if np.sign(r[i - 1]) == np.sign(r_decode[i - 1]):
                result[-1].insert(0, 1)
            else:
                result[-1].insert(0, 0)

    # Number of times an error was made for each user
    errors = [0] * N

    # Count the number of errors for each user
    for k in range(it):
        for i in range(N, 0, -1):
            errors[i - 1] += 1 - result[k][i - 1]

    errors = np.array(errors) / it
    return errors


if __name__ == '__main__':
    # Parameters for the simulation
    N = 9
    sigma = 0.5
    g = 0.5
    P = [5**k for k in range(N)]
    it = 10000

    print("N =", N)
    print("Powers =", P)
    print("Attenuation g =", g)
    print("Noise with a variance of σ² =", sigma**2)
    print("Simulation n = {} iterations".format(it))

    errors = stats(g, sigma, P, N, it) * 100  # Convert to %
    print(errors)

    # Plot the result
    plt.bar(range(1, N + 1), errors)
    plt.xlabel("User")
    plt.ylabel("Error percentage")
    plt.title("Estimation of the BER with {} iterations".format(it))
    plt.show()
