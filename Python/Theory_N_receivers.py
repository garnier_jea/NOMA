import math
import matplotlib.pyplot as plt
import numpy as np
import itertools

VERBOSE = False


def sgn(s, i):
    """
    Return 1 if i is in the list s, returns -1 otherwise
    """
    return 2 * (i in s) - 1


def inter(L1, L2):
    """
    Return the intersection between two lists
    """
    return set(L1).intersection(L2)


def Q(x):
    """
    Implementation of the Q-function
    """
    return (1 - math.erf(x / math.sqrt(2))) / 2


def theory(g, sigma, P, N, nuser=-1):
    """
    Computes the error probability for each index
    If nuser != -1, compute only for the user nuser (between 1 and N)
    """
    # All the power must be > 0
    if any(x < 0 for x in P):
        return None

    # Sigma must be > 0
    if sigma <= 0:
        return None

    # At least 1 user
    if N < 1:
        return None

    # Attenuation cannot be <= 0
    if g <= 0:
        return None

    # Generate all the possibilities for the signs
    S = list(itertools.product([0, 1], repeat=N))

    # List of probabilities for each user
    output = []

    # Compute each P_en
    for n in range(N, 0, -1):
        if VERBOSE:
            print("\n******* Computing for n = {} *******".format(n))

        # Generating all the possibilities of errors for n
        E_n = list(itertools.product([0, 1], repeat=(N - n)))

        # List of probabilities for the events
        Probas = []

        for s in S:
            # Find the indices with a positive sign
            signes = [i + 1 for i in range(len(s)) if s[i] == 1]

            # Find the indices with a negative sign
            signes_bar = [i + 1 for i in range(len(s)) if s[i] == 0]

            # Knowing the signs, the r_n can be computer
            r = [math.sqrt(P[i]) * sgn(signes, i + 1) for i in range(len(P))]

            for e in E_n:
                # Find the indices where an error is expected
                epsilon = [n + 1 + i for i in range(len(e)) if e[i] == 1]

                # Find the indices where there is no error
                epsilon_bar = [n + 1 + i for i in range(len(e)) if e[i] == 0]

                # List of constraints for the upper boundary of Re(n/g)
                Csup = []
                # List of constraints for the lower boundary of Re(n/g)
                Cinf = []

                # Compute the boundary for the index n
                if n in signes:
                    Csup.append(-sum(r[:n]) - 2 * sum([r[i - 1] for i in epsilon]))
                else:
                    Cinf.append(-sum(r[:n]) - 2 * sum([r[i - 1] for i in epsilon]))

                # Compute the boundaries for the other indices
                for i in inter(epsilon, signes):
                    Csup.append(-sum(r[:i]) - 2 * sum([r[j - 1] for j in inter(epsilon, range(i + 1, N + 1))]))

                for i in inter(epsilon_bar, signes):
                    Cinf.append(-sum(r[:i]) - 2 * sum([r[j - 1] for j in inter(epsilon, range(i + 1, N + 1))]))

                for i in inter(epsilon, signes_bar):
                    Cinf.append(-sum(r[:i]) - 2 * sum([r[j - 1] for j in inter(epsilon, range(i + 1, N + 1))]))

                for i in inter(epsilon_bar, signes_bar):
                    Csup.append(-sum(r[:i]) - 2 * sum([r[j - 1] for j in inter(epsilon, range(i + 1, N + 1))]))

                # Compute the value of the Q-function base on these contraints
                if len(Cinf) == 0 and len(Csup) == 0:
                    if VERBOSE:
                        print("S =", signes, "and ε =", epsilon, "⇒", "Error if Re(η/g) is in ]-∞ ; +∞[")
                        print("  P = 1")
                    Probas.append(1 / 2**N)
                elif len(Cinf) == 0:
                    if VERBOSE:
                        print("S =", signes, "and ε =", epsilon, "⇒", "Error if Re(η/g) is in ]-∞ ; {}]".format(min(Csup)))
                        print("  P =", 1 - Q(min(Csup) * (g / sigma)**2))
                    Probas.append(1 / 2**N * (1 - Q(min(Csup) * g / sigma)))
                elif len(Csup) == 0:
                    if VERBOSE:
                        print("S =", signes, "and ε =", epsilon, "⇒", "Error if Re(η/g) is in [{} ; +∞[".format(max(Cinf)))
                        print("  P =", Q(max(Cinf) * (g / sigma)**2))
                    Probas.append(1 / 2**N * Q(max(Cinf) * g / sigma))
                else:
                    if VERBOSE:
                        print("S =", signes, "and ε =", epsilon, "⇒", "Error if Re(η/g) is in [{} ; {}]".format(max(Cinf), min(Csup)))
                        print("  P =", Q(max(Cinf) * (g / sigma)**2) - Q(min(Csup) * (g / sigma)**2))
                    if max(Cinf) <= min(Csup):
                        Probas.append(1 / 2**N * (Q(max(Cinf) * g / sigma) - Q(min(Csup) * g / sigma)))
                    else:
                        Probas.append(0)

        # If the probability of a specific user is wanted
        if n == nuser:
            return sum(Probas)

        # Otherwise, append to the list
        output.insert(0, sum(Probas))

    # Return all the probabilities
    return output


if __name__ == '__main__':
    N = 9
    sigma = 0.5
    g = 0.5
    P = [5**k for k in range(N)]
    print("N =", N)
    print("Powers =", P)
    print("Attenuation g =", g)
    print("Noise with a variance of σ² =", sigma**2)

    probas = theory(g, sigma, P, N)
    probas = np.array(probas) * 100

    plt.bar(range(1, N + 1), probas)
    plt.xlabel("User")
    plt.ylabel("Error probability")
    plt.title("Theoretical error probability for each user")
    plt.show()
