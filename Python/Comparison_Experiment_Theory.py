import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import Theory_N_receivers as NOMATh


# Experimental parameters
N = 3
Pmax = 1
sigma = 0.05
g = 1

# Experimental measures
P1 = [0., 0., 0., 0.05, 0.1, 0., 0.05, 0.1, 0.15, 0., 0.05, 0.1, 0.15, 0.2, 0., 0.05, 0.1, 0.15, 0.2, 0.25, 0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0., 0.05, 0.1, 0.15, 0.2, 0., 0.05, 0.1, 0.]
P2 = [0., 0.05, 0.1, 0.1, 0.1, 0.15, 0.15, 0.15, 0.15, 0.2, 0.2, 0.2, 0.2, 0.2, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35, 0.4, 0.4, 0.4, 0.4, 0.4, 0.45, 0.45, 0.45, 0.5]
BER3 = [0, 0, 0, 0, 0, 0, 0, 0.004, 0.045, 0, 0.0005, 0.035, 0.16, 0.24, 0, 0.01, 0.14, 0.23, 0.24, 0.25, 0, 0.075, 0.22, 0.25, 0.25, 0.25, 0.25, 0, 0.19, 0.245, 0.26, 0.25, 0.25, 0.26, 0.02, 0.25, 0.25, 0.255, 0.25, 0.05, 0.24, 0.25, 0.25]
BER2 = [0.5, 0.005, 0.0001, 0.05, 0.25, 0, 0.015, 0.1, 0.3, 0, 0.002, 0.07, 0.3, 0.5, 0, 0.01, 0.15, 0.29, 0.4, 0.5, 0, 0.075, 0.22, 0.27, 0.31, 0.37, 0.5, 0, 0.19, 0.245, 0.27, 0.28, 0.3, 0.3, 0.02, 0.25, 0.25, 0.255, 0.27, 0.05, 0.24, 0.25, 0.25]
BER1 = [0.5, 0.5, 0.5, 0.05, 0.25, 0.5, 0.023, 0.1, 0.3, 0.5, 0.011, 0.07, 0.3, 0.5, 0.5, 0.02, 0.15, 0.28, 0.31, 0.26, 0.5, 0.085, 0.21, 0.16, 0.07, 0.11, 0.26, 0.5, 0.22, 0.16, 0.03, 0.03, 0.04, 0.25, 0.5, 0.13, 0.03, 0.01, 0.02, 0.5, 0.06, 0.03, 0.5]


def main():
    """
    Graph the theoritical error probability and experimental BER
    """
    # List of the sum of BERs for each parameter value
    BERTotalList = []
    BERTotalThList = []

    # List of the BER for 3rd user (highest power) for each parameter value
    BER3List = []
    BER3ThList = []

    # List of the BER for 2nd user for each parameter value
    BER2List = []
    BER2ThList = []

    # List of the BER for 1rst user (lowest power) for each parameter value
    BER1List = []
    BER1ThList = []

    for i in range(len(P1)):
        # Extract power frome experimental results
        p1 = P1[i]
        p2 = P2[i]
        p3 = Pmax - p2 - p1

        # Same with BER
        ber3 = BER3[i]
        ber2 = BER2[i]
        ber1 = BER1[i]

        # Now, computer the theoritical BER given the experimental conditions
        ber1th, ber2th, ber3th = NOMATh.theory(g, sigma, [p1, p2, p3], N)

        # Append all those values to the lists
        BERTotalList.append(ber1 + ber2 + ber3)
        BERTotalThList.append(ber1th + ber2th + ber3th)

        BER3List.append(ber3)
        BER3ThList.append(ber3th)

        BER2List.append(ber2)
        BER2ThList.append(ber2th)

        BER1List.append(ber1)
        BER1ThList.append(ber1th)

    # Plot in 3D the values of BER
    plt.figure()
    ax = plt.axes(projection="3d")
    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("Sum of BER")
    ax.set_title("Evolution of the total BER for N={} users, depending on the power, with Pmax = {}, g = {} and σ = {}".format(N, Pmax, g, sigma))
    ax.plot(P1, P2, BERTotalThList, markerfacecolor="r", marker="o", markersize=5, linestyle="None", label="Theoritical")
    ax.plot(P1, P2, BERTotalList, markerfacecolor="b", marker="x", markersize=5, linestyle="None", label="Measured")
    ax.legend(loc="lower right")
    plt.show(False)

    plt.figure()
    ax = plt.axes(projection="3d")
    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("BER3")
    ax.set_title("Evolution of the 3rd user's BER for N={} users, depending on the power, with Pmax = {}, g = {} and σ = {}".format(N, Pmax, g, sigma))
    ax.plot(P1, P2, BER3ThList, markerfacecolor="r", marker="o", markersize=5, linestyle="None", label="Theoritical")
    ax.plot(P1, P2, BER3List, markerfacecolor="b", marker="x", markersize=5, linestyle="None", label="Measured")
    ax.legend(loc="lower right")
    plt.show(False)

    plt.figure()
    ax = plt.axes(projection="3d")
    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("BER2")
    ax.set_title("Evolution of the 2nd user's BER for N={} users, depending on the power, with Pmax = {}, g = {} and σ = {}".format(N, Pmax, g, sigma))
    ax.plot(P1, P2, BER2ThList, markerfacecolor="r", marker="o", markersize=5, linestyle="None", label="Theoritical")
    ax.plot(P1, P2, BER2List, markerfacecolor="b", marker="x", markersize=5, linestyle="None", label="Measured")
    ax.legend(loc="lower right")
    plt.show(False)

    plt.figure()
    ax = plt.axes(projection="3d")
    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("BER1")
    ax.set_title("Evolution of the 1rst user's BER for N={} users, depending on the power, with Pmax = {}, g = {} and σ = {}".format(N, Pmax, g, sigma))
    ax.plot(P1, P2, BER1ThList, markerfacecolor="r", marker="o", markersize=5, linestyle="None", label="Theoritical")
    ax.plot(P1, P2, BER1List, markerfacecolor="b", marker="x", markersize=5, linestyle="None", label="Measured")
    ax.legend(loc="lower right")
    plt.show(True)


if __name__ == '__main__':
    main()
