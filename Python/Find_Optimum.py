import Theory_N_receivers as NOMATh
import numpy as np
import time
from scipy.optimize import minimize
from scipy.optimize import basinhopping
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm

"""
Minimize f(P)
Subject to:
    ∀ i ∈ ⟦1, N⟧, Pi ⩾ 0
    ∀ (i, j) ∈ ⟦1, N⟧², i ≤ j ⇒ Pi ≤ Pj
    ΣPi ≤ Pmax

It can be written as:
    min(f(P)) under the constraint that AX ⩾ B and ∀ i ∈ ⟦1, N⟧, Pi ⩾ 0

A = (-1,  1,  0, ...,  0,  0)
    ( 0, -1,  1, ...,  0,  0)
    ...
    ( 0,  0,  0, ..., -1,  1)
    (-1, -1, -1, ..., -1, -1)

X = t(P1, P2, ..., PN)

B = t(0, 0, ..., 0, -Pmax)

https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html
https://stackoverflow.com/a/52003654/3624018
"""

N = 3
Pmax = 10000
g = [1, 0.8, 0.65]
sigma = [5, 10, 15]
print("N = {}, Pmax = {}, g = {} and σ = {}".format(N, Pmax, g, sigma))


def graph_2D(n=100, block=False):
    """
    Graph the optimum for N = 2
    """
    # Generate all possible power repartitions and compute f(P)
    # If P1 > Pmax / 2, then P2 < P1. This cannot be, so we stop at Pmax / 2
    X = np.linspace(0, Pmax / 2, n)
    Y = [f([x, Pmax - x]) for x in X]

    # Plot those values
    ax = plt.axes()
    ax.plot(X, Y)
    ax.set_xlabel("P1")
    ax.set_ylabel("Sum of BER")
    ax.set_title("Evolution of the sum of BER for N={} users depending on the power, with Pmax = {}, g = [{:.3g}, {:.3g}] et σ = [{:.3g}, {:.3g}]".format(N, Pmax, g[0], g[1], sigma[0], sigma[1]))

    # Find the optimum
    opti = np.argmin(Y)
    P = [X[opti], Pmax - X[opti]]
    BERmin = Y[opti]
    BER1, BER2 = f2(P)
    print("Graphically, min was found for P1 = [{:.2g}, {:.2g}] with BER={:.3g}".format(P[0], P[1], BERmin))

    # Display the min we found
    ax.plot([P[0]], [BERmin], markerfacecolor="r", marker="o", markersize=5)
    ax.text(P[0], BERmin - 0.01, "P=({:.2g}, {:.2g}), BER1={:.2g}, BER2={:.2g}".format(P[0], P[1], BER1, BER2), fontsize=11, horizontalalignment="center", verticalalignment="center")

    # Display the output
    plt.show(block)

    # Return the value
    return P, BERmin


def graph_3D(step=Pmax / 100, block=False):
    """
    Graph the optimum for N = 3
    """
    X = []
    Y = []
    Z = []

    # Compute f(P) for all possible power repartitions
    # The constraints are Pi > 0 and Pi+1 < Pi < Pi-1
    # If P2 > Pmax / 2, then P3 < P2, which is impossible. So P2 stops at Pmax / 2
    P2 = 0
    while P2 <= Pmax / 2:
        P1 = 0
        # We want to stop before the sum of BER is greater than Pmax
        # Since P3 >= P2, the condition "P2 <= (Pmax - P2 - P1)" must be verified
        # so that, at worst, P3 = P2 and we don't use more than the allocated power
        while P1 <= P2 and P2 <= (Pmax - P2 - P1):
            P = [P1, P2, Pmax - P2 - P1]

            X.append(P1)
            Y.append(P2)
            Z.append(f(P))

            P1 += step

        P2 += step

    # Convert the lists to numpy arrays
    X = np.array(X)
    Y = np.array(Y)
    Z = np.array(Z)

    # Display all the datapoints
    ax = plt.axes(projection="3d")
    ax.scatter3D(X, Y, Z, c="blue")
    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("Sum of BER")
    ax.set_title("Evolution of the sum of BER for N={} users depending on the power, with Pmax = {}, g = [{:.3g}, {:.3g}, {:.3g}] and σ = [{:.3g}, {:.3g}, {:.3g}]".format(N, Pmax, g[0], g[1], g[2], sigma[0], sigma[1], sigma[2]))

    # Find the optimum
    opti = np.argmin(Z)
    P = [X[opti], Y[opti], Pmax - X[opti] - Y[opti]]
    BERmin = Z[opti]
    print("Graphically, a min was found for P = [{:.2g}, {:.2g}, {:.2g}] with BER={:.3g}".format(P[0], P[1], P[2], BERmin))

    # Display the min we found
    ax.plot([P[0]], [P[1]], [BERmin], markerfacecolor="r", marker="o", markersize=10)
    ax.text(P[0], P[1], BERmin - 0.05, "P=({:.2g}, {:.2g}, {:.2g}), BER={:.2g}".format(P[0], P[1], P[2], BERmin), fontsize=8, horizontalalignment="center", verticalalignment="center")

    # Display the output
    plt.show(block)

    # Return the value
    return P, BERmin


def graph_4D(step=Pmax / 100, block=False):
    """
    Graph the optimum for N = 4
    """
    X = []
    Y = []
    Z = []
    C = []
    ax = plt.axes(projection="3d")

    # Compute f(P) for all possible power repartitions
    # The constraints are Pi > 0 and Pi+1 < Pi < Pi-1
    # If P3 > Pmax / 2, then P4 < P3, which is impossible. So P3 stops at Pmax / 2
    P3 = P2 = P1 = 0
    while P3 <= Pmax / 2:
        P2 = 0
        # We want to stop before the sum of BER is greater than Pmax
        # Since P4 >= P3, the condition "P3 <= (Pmax P3 - P2 - P1)" must be verified
        # so that, at worst, P4 = P3 and we don't use more than the allocated power
        while P2 <= P3 and 2 * P3 + P2 + P1 <= Pmax:
            P1 = 0

            # Same as before, but with P1 and P2
            while P1 <= P2 and 2 * P3 + P2 + P1 <= Pmax:
                P = [P1, P2, P3, Pmax - P3 - P2 - P1]

                X.append(P1)
                Y.append(P2)
                C.append(P3)
                Z.append(f(P))

                P1 += step
            P2 += step
        P3 += step

    # Convert the lists to numpy arrays
    X = np.array(X)
    Y = np.array(Y)
    Z = np.array(Z)
    C = np.array(C)

    # Find the min and max values for BER (aka the color)
    Cmax = max(C) + 0.1
    Cmin = min(C) - 0.1

    # Display all the datapoints
    # Use colors for the 4th dimension
    for i in range(len(X)):
        x = X[i]
        y = Y[i]
        z = Z[i]
        color = [0, 0, (Cmax - C[i]) / (Cmax - Cmin)]
        ax.plot([x], [y], [z], c=color, marker=".", markersize=10, alpha=0.7)

    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("Sum of BER")
    ax.set_title("Evolution of the sum of BER for N={} users depending on the power, with Pmax = {}, g = [{:.3g}, {:.3g}, {:.3g}, {:.3g}] et σ = [{:.3g}, {:.3g}, {:.3g}, {:.3g}]".format(N, Pmax, g[0], g[1], g[2], g[3], sigma[0], sigma[1], sigma[2], sigma[3]))
    print("Graphically, a min was found for P = [{:.2g}, {:.2g}, {:.2g}, {:.2g}] with BER={:.3g}".format(P[0], P[1], P[2], P[3], BERmin))

    # Display a colorbar
    sm = cm.ScalarMappable(cmap=cm.Blues, norm=plt.Normalize(vmin=0, vmax=1))
    sm._A = []
    cbar = plt.colorbar(sm)
    cbar.set_label("P3", rotation=0)

    # Find the optimum
    opti = np.argmin(Z)
    P = [X[opti], Y[opti], C[opti], Pmax - X[opti] - Y[opti] - C[opti]]
    BERmin = Z[opti]

    # Display the min we found
    ax.plot([P[0]], [P[1]], [BERmin], markerfacecolor="r", marker=".", markersize=15)
    ax.text(P[0], P[1], BERmin - 0.05, "P=({:.2g}, {:.2g}, {:.2g}, {:.2g}), BER={:.2g}".format(P[0], P[1], P[2], P[3], BERmin), fontsize=8, horizontalalignment="center", verticalalignment="center")

    # Display the output
    plt.show(block)

    # Return the value
    return P, BERmin


def evaluate_min(step=Pmax / 100):
    """
    Find the min for more than 4 users
    """
    global nbrSteps, BERmin, Pmin

    # Init default values
    Pmin = [0] * (N - 1)
    BERmin = f(Pmin + [Pmax])

    # Init global variable to count the number of steps this took
    nbrSteps = 1

    def should_continue(P, i, step):
        """
        Returns True if another value of power for i should be tested
        """
        # Make sure we don't use more than the max allocated power
        if sum(P) + P[-1] + step > Pmax:
            return False

        # Make sure the power remains smaller than the one of the previous user
        if i == len(P) - 1:
            # This is the first-but-one user (the last user's power is not in P since their power can be deduced)
            # Just don't take-up more than half of the total power
            return P[i] <= Pmax / 2 - step
        else:
            # Make sure that, after adding step, the power is still smaller than the one of the previous user
            return P[i] <= P[i + 1] - step

    def rec(P, i, step):
        """
        Recursive method to compute all the BER values
        """
        global nbrSteps, BERmin, Pmin

        # Make sure the user is valid
        if i < 0:
            return

        while should_continue(P, i, step):
            # Increment the power
            P[i] += step
            nbrSteps += 1

            # Compute the new BER
            BER = f(P + [Pmax - sum(P)])

            # Update the min BER if necessary
            if BER < BERmin:
                BERmin = BER
                Pmin = P.copy()  # Make sure to copy the list as it'll be modified in the future

            # Continue for the next user
            rec(P.copy(), i - 1, step)

    # Start the recursive method
    rec(Pmin.copy(), N - 2, step)

    # Return the result
    print("Iterativally, a min was found for P = {} in {} iterations".format(Pmin + [Pmax - sum(Pmin)], nbrSteps))
    return Pmin + [Pmax - sum(Pmin)], BERmin


def global_min(x0, bounds, constraints=None, niter=100):
    """
    Find the global min using the basinhopping method
    """
    # Stepsize must be big enough so it doesn't get stuck in a local min
    ret = basinhopping(f, x0=x0, niter=niter, stepsize=Pmax / 10, minimizer_kwargs={"bounds": bounds, "constraints": constraints})
    print("The global min was found for P =", ret.x)
    print("BER =", f(ret.x))


def local_min(x0, bounds, constraints=None, method="SLSQP"):
    """
    Find a local min using minimize
    """
    if method == "COBYLA":
        # Cobylla can't handle bounds (so power might go under 0)
        ret = minimize(f, x0=x0, method=method, constraints=constraints)
    else:
        ret = minimize(f, x0=x0, bounds=bounds, method=method, constraints=constraints)

    print("A local min was found for P =", ret.x)
    print("BER =", f(ret.x))


def f(P):
    """
    Function to minimize
    Input: List of powers (by order of increasing value)
    Output: total BER (float)
    """
    # Each user having a different g and sigma, the probability must be recomputed each time
    probas = []
    for i in range(N):
        # Compute the theoritical probability of error
        proba = NOMATh.theory(g[i], sigma[i], P, N, nuser=i + 1)

        # A probability of "None" means there's a problem with one parameter, so return +inf
        if proba is None:
            return np.inf
        else:
            probas.append(proba)

    # Compute and return the total BER
    return sum(probas)


def constraint(P):
    """
    Function which checks that the constraints are being respected
    Input: List of powers (by order of increasing value)
    Output: Matrix in which all values must be >= 0
    """
    R = np.matmul(A, P.T)
    return R - B


# Creating matrix A (see comment at the top of the script)
A = np.zeros((N, N))

for i in range(N - 1):
    A[i, i] = -1
    A[i, i + 1] = 1

A[-1, :] = -np.ones((1, N))

# Creating matrix B (see comment at the top of the script)
B = np.zeros(N)
B[-1] = -Pmax

# Dict indicating that our function "constraints" should be used
cons = {"type": "ineq", "fun": constraint}  # ineq: the constraint function result is to be non-negative

# Creating the bounds for all the values or power (they must be between 0 and +∞)
# Note: may not be useful since f(P) returns +∞ for negative power values
bounds = [(0, np.inf)] * N

# First methon: Évaluating f(P) in multiple values, finding the min, and then starting from there to find the local min
print("----- Finding graphical minimum -----")
t0 = time.time()
if N == 2:
    Pmin, BERmin = graph_2D(n=200)
elif N == 3:
    Pmin, BERmin = graph_3D(step=Pmax / 100)
elif N == 4:
    Pmin, BERmin = graph_4D(step=Pmax / 50)
else:
    Pmin, BERmin = evaluate_min(step=Pmax / 25)

print("Finding local min starting with this point...")
local_min(Pmin, bounds, constraints=cons)

print("Took {:.2g}s".format(time.time() - t0))

# Second method: Finding global min with the basinhopping method
print("----- Finding global minimum -----")
x0 = np.zeros(N)
niter = 20
print("P0 =", x0)

t0 = time.time()
global_min(x0, bounds, constraints=cons, niter=niter)
print("Took {:.2g}s for {} iterations".format(time.time() - t0, niter))

# Third method: finding the local min starting with an arbitrary point
print("----- Finding local minimum with arbitrary starting point -----")

# Choosing the initial starting point is important
# Here, we decide that each user should use twice as much power as the user before them
x0 = np.array([2**i for i in range(N)], dtype="float64")
x0 *= Pmax / sum(x0)
print("P0 =", x0)

t0 = time.time()
local_min(x0, bounds, constraints=cons)
print("Took {:.2g}s".format(time.time() - t0))

# Display the plot and prevent Python from closing if necessary
if N < 5:
    plt.show(True)
