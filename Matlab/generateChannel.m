function [hMatrix, H] = generateChannel(model, fs, timeSpan, fd, Ts)

%GENERATECHANNEL generate Random Channel Impulse Response Based on tapped
%   delay line models, taking into account time variation caused by Doppler
%   effect.
%
%   Available channel models and their abbreviations
%       - Pedestrian A & B      [PedA, PedB]
%       - Vehicular A & B       [VehA, VehB]
%       - Hilly Terrain         [HT]
%       - Typical Urban         [TU]
%       - Rural Area            [RA]
%       - Ext Ped.A, Veh.A, TU  [EPA, EVA, ETU]
%       - Indoor Office A & B   [IOA, IOB]
%
%   Inputs :
%   model       : channel model abbriviation from list above
%   fs          : sampling frequency (Hz)
%   timeSpan    : time length of the channel response to generate (s)
%   fd          : doppler frequency (Hz)
%   Ts          : Time-Symbol (s) - used to know how many coeffs to compute
%
%   Outputs :
%   hMatrix     : matrix of channel time responses. Each line corresponds
%                 to the reponse on one Ts. If Ts = timespan, hMatrix has
%                 only one line
%   H           : matrix of channel frequency responses. Each line
%                 corresponds to the frequency response of the channel 
%                 during one Ts

%   Other m-files required: none
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   Based on Dr. Vincent Savaux's PhD. Thesis, section 1.4, "Simulation of
%   the transmission channel" :
%   "Vincent Savaux. Contribution a l�estimation de canal multi-trajets
%   dans un contexte de modulation OFDM. Other. Supelec, 2013. French."
%   See at : https://goo.gl/b23Tob

%   Author  : Quentin BODINIER
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : quentin.bodinier@supelec.fr
%   Website : quentinbodinier.wordpress.com
%
%   Sep. 2015; Last revision : 01/02/2016

dopplerModels = {'Jakes','Flat'};
switch model
    case 'PedA'
        delays = 1e-9.*[0 110 190 410];
        powers = 10.^(1/10.*[0 -9.7 -19.2 -22.8]);
        doppler = [1 1 1 1];
    case 'PedB'
        delays = 1e-9.*[0 200 800 1200 2300 3700];
        powers = 10.^(1/10.*[0 -0.9 -4.9 -8.0 -7.8 -23.9]);
        doppler = [1 1 1 1 1 1];
    case 'VehA'
        delays = 1e-9.*[0 310 710 1090 1730 2510];
        powers = 10.^(1/10.*[0 -1 -9 -10 -15 -20]);
        doppler = [1 1 1 1 1 1];
    case 'EVA'
        delays = 1e-9.*[0 30 150 310 370 710 1090 1730 2510];
        powers = 10.^(1/10.*[0 -1.5 -1.4 -3.6 -0.6 -9.1 -7.0 -12.0 -16.9]);
        doppler = [1 1 1 1 1 1 1 1 1];
    case 'EPA'
        delays = 1e-9.*[0 30 70 90 110 190 410];
        powers = 10.^(1/10.*[0 -1 -2 -3 -8 -17.2 -20.8]);
        doppler = [1 1 1 1 1 1 1];
    case 'TU'
        delays = 1e-9.*[0 50 120 200 230 500 1600 2300 5000];
        powers = 10.^(1/10.*[-1 -1 -1 0 0 0 -3 -5 -7]);
        doppler = [1 1 1 1 1 1 1 1 1];
    case 'VehB'
        delays = 1e-9.*[0 300 8900 12900 17100 20000];
        powers = 10.^(1/10.*[-2.5 0 -12.8 -10.0 -25.2 -16]);
        doppler = [1 1 1 1 1 1];
    case 'HT'
        delays = 1e-9.*[0 356 441 528 546 609 625 842 916 941 15000 16172 16492 16876 16882 16978 17615 17827 17849 18016];
        powers = 10.^(1/10.*[-3.6 -8.9 -10.2 -11.5 -11.8 -12.7 -13.0 -16.2 -17.3 -17.7 -17.6 -22.7 -24.1 -25.8 -25.8 -26.2 -29.0 -29.9 -30.0 -30.7]);
        doppler = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
    case 'ETU'
        delays = 1e-9.*[0 217 512 514 517 674 882 1230 1287 1311 1349 1533 1535 1622 1818 1836 1884 1943 2048 2140];
        powers = 10.^(1/10.*[-5.7 -7.6 -10.1 -10.2 -10.2 -11.5 -13.4 -16.3 -16.9 -17.1 -17.4 -19.0 -19.0 -19.8 -21.5 -21.6 -22.1 -22.6 -23.5 -24.30]);
        doppler = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
    case 'RA'
        delays = 1e-9.*[0 42 101 129 149 245 312 410 469 528];
        powers = 10.^(1/10.*[-5.2 -6.4 -8.4 -9.3 -10 -13.1 -15.3 -18.5 -20.4 -22.4]);
        doppler = [1 1 1 1 1 1 1 1 1 1];
    case 'IOA'
        delays = 1e-9.*[0 50 110 170 290 310];
        powers = 10.^(1/10.*[0 -3 -10 -18 -26 -32]);
        doppler = [2 2 2 2 2 2];
    case 'IOB'
        delays = 1e-9.*[0 100 200 300 500 700];
        powers = 10.^(1/10.*[0 -3.6 -7.2 -10.8 -18.0 -25.2]);
        doppler = [2 2 2 2 2 2];       
    otherwise
        error('Channel model unknown');
end

% Time vector definition
t = 0:1/fs:timeSpan;

% Return error if timeSpan is too small
if max(t) < Ts
    error('Provided timeSpan should be higher than Ts')
end

% Number of symbols that the signal will spread
Nsymbols = floor(timeSpan/Ts);

% Frequency vector
f = -1/(2*Ts):1/Ts*1/Nsymbols:1/(2*Ts)-1/Ts*1/Nsymbols;

% Random coeffs generation
coeffMatrix = 1/sqrt(2).*(randn(Nsymbols,length(delays))+1j.*randn(Nsymbols,length(delays)));
for i = 1:Nsymbols
    coeffMatrix(i,:) = sqrt(powers).*coeffMatrix(i,:);
    coeffMatrix(i,:) = 1/sqrt(var(coeffMatrix(i,:))).*coeffMatrix(i,:);
end
H = fft(coeffMatrix);

% Doppler spectrum definition
fOI = find(abs(f)<fd-0.00001);
S_Jakes = zeros(size(f));
S_flat  = zeros(size(f));
S_Jakes(fOI) = 1./(pi.*fd.*sqrt(1-(f(fOI)/fd).^2));
S_Jakes = transpose(sqrt(S_Jakes));
S_flat(fOI) = 1/(2*fd);
S_flat = transpose(sqrt(S_flat));
if fd==0
    S_Jakes(ceil(length(f)/2)) = 1;
    S_flat(ceil(length(f)/2)) = 1;
end

S = {S_Jakes, S_flat};

 % Doppler application
 for i = 1:length(delays)
     H(:,i) = S{doppler(i)}.*H(:,i);
 end
cMtxPostDoppler = ifft(H);

% Building of the channel time response matrix
for i = 1:length(delays)
    [~, ind(i)] = min(abs(t-delays(i)));
end

hMatrix = zeros(Nsymbols, floor(Ts*fs));

for n=1:Nsymbols
    hMatrix(n,ind) = hMatrix(n,ind)+cMtxPostDoppler(n,:);
    hMatrix(n,:) = hMatrix(n,:)./norm(hMatrix(n,:));
end

H = transpose(fft(transpose(hMatrix)));
end
