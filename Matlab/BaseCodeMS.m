clear all;
close all;
clc;

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

% Parameters you can change
N_user_General              = 2; % Number of users
N_canaux_General             = 4; % Number of channels
distances_General      = [400 400]; % distance between each user and the base station 


% Parameters you can change sometimes
Wmodel          = 'B1';
CModel          = 'ETU';        % extended typical urban

Antenna_gain    = 10; % Antenna gain in dB
Noise_fig       = 2; % Noise figure
Nth             = -121.44; % thermal noise 

% Parameters you do no have to change
frequency       = 2*10^9;
Nb_sub          = 12;           % number of subcarriers per ressource block
Ts              = 1/15000;      % Time symbol in LTE
fs              = Nb_sub*N_canaux_General/Ts;      % Sampling frequency in Hz, here case of LTE with 2048 subC (only 1200 subcarriers are used)
duration        = 2*Ts;         % Duration of the simulation in s
fd              = 0;            % dopler frequency

% Bandwidth of one channel
B_c            = Nb_sub/Ts; % Cest bien B_c ? 

C_k_General=10^6*[0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5];
N_C_k_k=size(C_k_General,2);
    
N_user=N_user_General;
N_canaux=N_canaux_General;
distances=distances_General(1,1:N_user);

%%%%%%%%%%%%%%%%
% COMPUTATIONS %
%%%%%%%%%%%%%%%%

% Computation of the channel gains
[PL]            = channelgain_Winner(Wmodel,distances_General,frequency);
g_moy           = 10.^((Antenna_gain-PL)/10);

% We now compute the channel gain per user per subcarrier
H_u             = zeros(N_user_General,fs*Ts);
for j=1:1:N_user_General
    [hMatrix, H]            = generateChannel(CModel, fs, duration, fd, Ts);
	H_norm                  = abs(H(1,:));
	H_u(j,:)                = g_moy(1,j)*H_norm;
end


% The channel coefficient g is the average of the channel coefficients in
% every channel (12 subcarriers)


g               = zeros(size(H_u,1),size(H_u,2)/Nb_sub);

for j=1:1:N_canaux_General
   for k=1:1:N_user_General
        g(k,j)    = (prod(H_u(k,(j-1)*Nb_sub+1:j*Nb_sub)))^(1/Nb_sub);
   end    
end

% Noise
N_dB            = Nth+Noise_fig+10*log10(B_c);
N               = 10^(N_dB/10);